import React, { Component } from "react";
import { StyleSheet, View, Image } from "react-native";
import MaterialButtonLight from "../components/components/MaterialButtonLight";
import CupertinoButtonDanger from "../components/components/CupertinoButtonDanger";
import logoV from '../assets/images/logoV_(1).png'




class TelaPrincipal extends React.Component {

  onPressCadastrar(){
    this.props.navigation.navigate('Registro')
  }
  render(){
    
    return (
      <View style={styles.container}>
        <MaterialButtonLight onPress={() => this.props.navigation.navigate('Registro')}
          style={styles.materialButtonLight}
        ></MaterialButtonLight>
        <CupertinoButtonDanger
          onPress={() =>
            this.props.navigation.navigate('TelaLogin')}
          style={styles.cupertinoButtonDanger}
        ></CupertinoButtonDanger>
        <Image
          source={logoV}
          resizeMode="contain"
          style={styles.image}
        ></Image>
      </View>
    );
  }
}



const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center'
  },
  materialButtonLight: {
    height: 47,
    width: 310,
    marginTop: 677,
  },
  cupertinoButtonDanger: {
    height: 47,
    width: 310,
    marginTop: -126,
  },
  image: {
    width: 281,
    height: 297,
    marginTop: -574,
  }
});

export default TelaPrincipal;
