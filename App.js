import React from 'react';

import { createStackNavigator } from "@react-navigation/stack";
import { NavigationContainer } from '@react-navigation/native'
import { useFonts } from 'expo-font';


import TelaPrincipal from './screens/TelaPrincipal'
import { Restaurant, OrderDelivery } from './screens'
import Tabs from './navigation/tabs'
import Profile from './screens/Profile';
import TelaLogin from './screens/TelaLogin'
import Register from './screens/Register';

const Stack = createStackNavigator();

const App = () => {

    const [loaded] = useFonts({
      "Roboto-Black" : require('./assets/fonts/Roboto-Black.ttf'),
      "Roboto-Bold" : require('./assets/fonts/Roboto-Bold.ttf'),
      "Roboto-Regular" : require('./assets/fonts/Roboto-Regular.ttf'),

    })
    
    if(!loaded){
      return null;
    }
    
    
      return (
          <NavigationContainer>
              <Stack.Navigator
                  screenOptions={{
                      headerShown: false
                  }}
                  initialRouteName={'TelaPrincipal'}
              >   
                  <Stack.Screen name='TelaPrincipal' component={TelaPrincipal}/>
                  <Stack.Screen name='TelaLogin' component={TelaLogin}/>
                  <Stack.Screen name='Registro' component={Register}/>
                  <Stack.Screen name="Home" component={Tabs} />
                  <Stack.Screen name="Restaurant" component={Restaurant} />
                  <Stack.Screen name="OrderDelivery" component={OrderDelivery} />
                  <Stack.Screen name="Profile" component={Profile}/>
                  
              </Stack.Navigator>
          </NavigationContainer>
      )
    
}

export default App;